import {Card, Button, Row, Col} from 'react-bootstrap'

export default function Course(){
	return(
		<Row>
			<Col>
				<Card className="cardCourse">
					<Card.Body>
						<Card.Title>
							<h2>Japanese101</h2>
						</Card.Title>
						<Card.Text>
							<h3>Description</h3>
							<p>
							This class is for beginner level Japanese Speakers.
							</p>
							<h4>Price</h4>
							<p>2500</p>
							<Button className="btn-primary">Enroll</Button>
						</Card.Text>	
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}